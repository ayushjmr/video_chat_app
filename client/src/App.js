import React from 'react';

import Chat from './components/Chat/Chat';
import JoinRoom from './components/JoinRoom/JoinRoom';
import JoinVideoRoom from './components/JoinVideoRoom/JoinVideoRoom';

import AgentVideoPage from './components/AgentVideoPage/AgentVideoPage';
import ClientVideoPage from './components/ClientVideoPage/ClientVideoPage';

import { BrowserRouter as Router, Route } from "react-router-dom";

const App = () => {
  console.log(ClientVideoPage);
  return (
    <Router>
      {/* <Route path="/chat" component={Chat} /> */}
      
      <Route path="/chat-client" component={ClientVideoPage} />
      {/* <Route path="/chat-agent" component={AgentVideoPage} /> */}
     
      {/* <Route path="/" exact component={JoinRoom} /> */}
      {/* <Route path="/" exact component={JoinVideoRoom}/> */}

    </Router>

  );
}

export default App;
