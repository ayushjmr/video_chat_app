import React, { useState } from 'react';
import { Link } from "react-router-dom";

import './JoinVideoRoom.css';
// import AgentVideoPage from '../AgentVideoPage/AgentVideoPage';
// import ClientVideoPage from '../ClientVideoPage/ClientVideoPage';

export default function SignInVideo() {
    // nameac => agent client name

  const [agentorclient, setAgentorclient] = useState('');
  const [nameac, setNameac]= useState('');
  const [room, setRoom] = useState('');
  const [id, setId] = useState('');
  const [priority, setPriority] = useState('');
  const [language, setLanguage] = useState('');



  //  function func(agentorclient){
  //   console.log("sdddsfsdfsd");
 
  //   var string1=agentorclient;
  //   var string2 = "client";
  //   var result = string1.localeCompare(string2);
  //   console.log(result);
  //   if(result===0)
  //     {
  //       // return <h1>Welcome User</h1>; 
  //       // return <ClientVideoPage />;
       
  //     }
  //     else{
  //       return <h1>Welcome agent</h1>; 
  //       // return <AgentVideoPage />;
  //     }

  //  }


  return (
    <div className="joinOuterContainer">
      <div className="joinInnerContainer">
        <h1 className="heading">Join Video</h1>

        <div>
          <input placeholder="Client or Agent? " className="joinInput" type="text" onChange={(event) => setAgentorclient(event.target.value)} required />
        </div>
        <div>
          <input placeholder="Id" className="joinInput  mt-20" type="text" onChange={(event) => setId(event.target.value)} required/>
        </div>
        <div>
          <input placeholder="Name" className="joinInput  mt-20" type="text" onChange={(event) => setNameac(event.target.value)} required/>
        </div>

        <div>
          <input placeholder="Priority" className="joinInput  mt-20" type="text" onChange={(event) => setPriority(event.target.value)} required/>
        </div>
        <div>
          <input placeholder="Language" className="joinInput  mt-20" type="text" onChange={(event) => setLanguage(event.target.value)} />
        </div>

        <div>
          <input placeholder="Room" className="joinInput mt-20" type="text" onChange={(event) => setRoom(event.target.value)} required/>
        </div>
           {/* agentorclient,Id,name,prioority,language,room */}                                                                                                                                                      
        
        <Link to={`/chat-${agentorclient}&id=${id}&nameac=${nameac}&priority=${priority}&language=${language}&room=${room}`}>
          
          <button className={'button mt-20'} type="submit" >Enter</button>
        </Link>

        
         
      </div>
  </div>

  );
}

