import React from 'react';

import onlineIcon from '../../icons/gif1.gif';
import closeIcon from '../../icons/closeIcon.png';

import './ChatBar.css';

const ChatBar = ({ room }) => (
  <div className="chatBar">
    <div className="leftInnerContainer">
      {/* <img className="onlineIcon"  src={onlineIcon} alt="online icon" /> */}
        <img className="onlineIcon"  src={onlineIcon}  alt="online icon" />

      <h3>Room {room}</h3>

    </div>
    
    <div className="rightInnerContainer">
      <a  href="/"><img width="30px" height="30px" src={closeIcon} alt="close icon" /></a>
    </div>
  </div>
);

export default ChatBar;